using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Refactoring
{
    public class DataProcessorTests
    {
        // minimum prices

        [Fact]
        void discountsOverruledByMinimumPriceForCSD()
        {

            // Arrange
            Item i1 = new Item(32, 50, 20, true, "CSD", 1200);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(900, items[0].price);
        }

        [Fact]
        void discountsAllowedWhenOverMinimumPriceForCSD()
        {

            // Arrange
            Item i1 = new Item(32, 50, 20, true, "CSD", 3000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.True(items[0].price > 900);
        }

        [Theory]
        [InlineData(1000)]
        [InlineData(1500)]
        void discountsOverruledByMinimumPriceForCSM(int initialFullPrice)
        {

            // Arrange
            Item i1 = new Item(32, 50, 20, true, "CSM", initialFullPrice);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(1000, items[0].price);
        }

        [Fact]
        void discountsAllowedWhenOverMinimumPriceForCSM()
        {

            // Arrange
            Item i1 = new Item(32, 50, 20, true, "CSM", 3000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.True(items[0].price > 1000);
        }

        [Theory]
        [InlineData(1000)]
        [InlineData(1700)]
        void discountsOverruledByMinimumPriceForCSPO(int initialFullPrice)
        {

            // Arrange
            Item i1 = new Item(32, 50, 20, true, "CSPO", initialFullPrice);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(1200, items[0].price);
        }

        [Fact]
        void discountsAllowedWhenOverMinimumPriceForCSPO()
        {

            // Arrange
            Item i1 = new Item(32, 50, 20, true, "CSPO", 2001);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.True(items[0].price > 1200);
        }

        // full prices

        [Theory]
        [InlineData("CSD")]
        [InlineData("CSPO")]
        [InlineData("CSM")]
        void shouldHaveFullPrice_theDayBeforeTraining(String trainingCourse)
        {

            // Arrange
            Item i1 = new Item(2, 50, 20, true, trainingCourse, 4000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(4000, items[0].price);
        }

        // full prices when few seats left close to training course

        [Theory]
        [InlineData("CSD")]
        [InlineData("CSPO")]
        [InlineData("CSM")]
        void whenFewSeatsLeftCloseToTrainingDate_shouldHaveFullPrice(String trainingCourse)
        {

            // Arrange
            Item i1 = new Item(6 /* days left */, 50, 3 /* seats left */, true, trainingCourse, 2000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(2000, items[0].price);
        }

        // Proportional discount 10 days before the training course

        [Fact]
        void shouldApplyProportionalDiscountForCSD()
        {

            // Arrange
            int daysBeforeProcessing = 11;
            Item i1 = new Item(daysBeforeProcessing /* days left */, 50, 5, true, "CSD", 2000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            int expectedPrice = 2000 - 10 * 30;
            Assert.Equal(expectedPrice, items[0].price);
        }

        [Theory]
        [InlineData("CSPO")]
        [InlineData("CSM")]
        void shouldApplyProportionalDiscountForCSMandCSPO(String trainingCourse)
        {

            // Arrange
            int daysBeforeProcessing = 11;
            Item i1 = new Item(daysBeforeProcessing /* days left */, 50, 5, true, trainingCourse, 2000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            int expectedPrice = 2000 - 10 * 20;
            Assert.Equal(expectedPrice, items[0].price);
        }

        // discount when 20 or less days before the training course

        [Fact]
        void when20orLessDaysBeforeTraining_shouldApplyDiscountForCSD()
        {

            // Arrange
            Item i1 = new Item(21, 50, 5, true, "CSD", 2000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(2000 - 500, items[0].price);
        }

        [Theory]
        [InlineData("CSPO")]
        [InlineData("CSM")]
        void when20orLessDaysBeforeTraining_shouldApplyDiscountForCSMandCSPO(String trainingCourse)
        {

            // Arrange
            Item i1 = new Item(21, 50, 5, true, trainingCourse, 2000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(2000 - 400, items[0].price);
        }

        // discount when more than 20 days before the training course

        [Fact]
        void whenMoreThan20DaysBeforeTraining_shouldApplyDiscountForCSPO()
        {

            // Arrange
            Item i1 = new Item(22, 50, 5, true, "CSPO", 4000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(4000 - 700, items[0].price);
        }

        [Theory]
        [InlineData("CSD")]
        [InlineData("CSM")]
        void whenMoreThan20DaysBeforeTraining_shouldApplyDiscountForCSDandCSM(String trainingCourse)
        {

            // Arrange
            Item i1 = new Item(22, 50, 5, true, trainingCourse, 2000);
            List<Item> items = new List<Item> { i1 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(2000 - 600, items[0].price);
        }



        // total current value of training courses 

        [Fact]
        void should_calculate_total_value_of_remaining_training_courses()
        {

            Item i1 = new Item(30, 50, 5, true, "CSD", 4000);
            Item i2 = new Item(15, 50, 1, true, "CSD", 2000);

            List<Item> items = new List<Item> { i1, i2 };

            // Act
            items = DataProcessor.ProcessData(items);

            // Assert
            Assert.Equal(3400 * 5 + 1500 * 1, DataProcessor.value);

        }
    }
}
