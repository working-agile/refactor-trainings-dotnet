using System;

namespace Refactoring
{
    public class Item
    {
        public int d { set; get; }
        public int s { set; get; }
        public int a { set; get; }
        public Boolean online { set; get; }
        public String type { set; get; }
        public int price { set; get; }
        public int full { set; get; }

        public Item(int d, int s, int a, Boolean online, String type, int full)
        {
            this.d = d;
            this.s = s;
            this.a = a;
            this.online = online;
            this.type = type;
            this.price = full;
            this.full = full;
        }
    }
}